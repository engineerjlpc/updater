# TapBit

TapBit lets you keep track of tasks that you are working on and helps motivate you to get them done. The final product will include a physical USB device that helps you interact with the software.

The project is currently in Alpha testing and is not a finished product. Feedback is massively important at this stage and any constructive criticism is welcome!

Updates are provided very frequently and it is suggested you install them as soon as they are released.

# Upcoming features
- [x] Add panning and zooming to chart view
- [x] Add/edit sessions manualy by right-clicking within the chart
- [ ] Change/pin window location
- [ ] Option to mark tasks as complete
- [ ] Let the user change category colours

# Installing
*If you get a security message from Windows when you try to run TapBit you should select More info -> Run anyway. This software is 100% safe and is simply unknown to Windows.*

*You may also get a security message from you anti-virus software, in which case you should add TapBit to the safe list.*

**Fresh Install**
 1. Download the latest version by clicking on the .zip file above
 2. Unzip and move the folder to a safe place (e.g. My Documents)
 3. Right-click TapBit.exe and create a shortcut
 4. Move this shortcut to the Desktop (don't move the .exe itself as it needs the other files in the same folder to work)
 5. Double click the shortcut to run

**Just Updating**

Simply unzip and replace only the old version of TapBit.exe with the new version

# Tutorial
- Each task has a name (e.g. write essay, finish homework, revise chapter 2) and is assigned to a category which could be a course subject or project (e.g. Economics, Philosophy, Cool Project)
- You can add tasks and categories from the main window and edit them later in Settings
- Your progress is recorded by completing sessions which are assigned to a specific task
- To start a session press the Start Work button
- To end a session and save it press the End Session button
- During a session TapBit will remind you to take breaks to help you keep focused and motivated
- All your data (settings, tasks, and sessions) are saved in a file called MyDatabase.db which is created in the same folder as TapBit.exe
- Past sessions can be viewed on a timeline by pressing the Histoy button.
- You can add and edit sessions manualy by right clicking within the chart

Copyright © 2018 J.L.P.C All Rights Reserved


